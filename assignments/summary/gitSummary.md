*Introduction*-

 >Git is basically a software where developers can easily collaborate with their teammates easily.  
 >It enables us to manage and oversee every change, dvelopemant or modification in the code by them or their teammates.

*Terms and Vocabulary*-

* Repository or repo: its the collection of files and folders of out code.
* GitLab: The second most popular remote storage solution for git repos.
* Commit: Its basically saving our work in our local machine(unless pushed to a remote repo).
* Push: Pushing is saving and syncing our saved work or commits to GitLab or the remote repo.
* Branch: The main software is the Master brancha and seperate instances of the code are branches which are once finalised, *MERGE*d into the main branch.
* Clone: clones or copies the selected online repo to our local machine.
* Forking: Similar to cloning but intead of duplicating it creates a new repo of the same content under our own name.

*Internals*-

 Three main states of our files:
  1. Modified means changes exist in our file but have not been commited or saved yet.
  2. Staged means that we have marked a modified file in its current version to go into our next picture/snapshot.
  3. Committed means that the data is safely stored in your local repo in form of pictures/snapshots
 At a time, there are 3 or 4 different trees of our software code/repository present:
  1. Workspace: All the changes made via Editor(s) (gedit, notepad, vim, nano) is done in this tree of repo.
  2. Staging: All the staged files go into this tree of our repo.
  3. Local Repository: All the (only)committed files go to this tree of our repo.
  4. Remote Repository: This is the copy of your Local Repo but is stored in a server on the Internet(commit and push).
![workflow](https://docs.gitlab.com/ee/topics/img/gitlab_flow_four_stages.png)  
  
*Git Workflow*-

 1. Clone the repo
 2. Create new branch
 3. Modify diles in working tree.
 4. Selectively stage just those changes you want to be part of your next commit.
 5. Do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Local Git Repo.
 6. Then push it to the master repo or the cloud.
 


















